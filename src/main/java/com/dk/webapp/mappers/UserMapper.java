package com.dk.webapp.mappers;

import com.dk.webapp.dto.CreateOrUpdateUserDto;
import com.dk.webapp.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toEntity(CreateOrUpdateUserDto createOrUpdateUserDto);

    CreateOrUpdateUserDto toDto(User user);
}
