package com.dk.webapp.dto;

import com.dk.webapp.model.Address;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;

@Getter
@Setter
public class CreateOrUpdateUserDto {

    private Long id;

    private String login;

    private String firstName;

    private String lastName;

    private String email;

    private LinkedList<Address> addresses;

    private String password;

    private String confirmPassword;



}
