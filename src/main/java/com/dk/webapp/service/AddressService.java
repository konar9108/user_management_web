package com.dk.webapp.service;

import com.dk.webapp.exception.ModelNotFoundException;
import com.dk.webapp.model.Address;
import com.dk.webapp.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address findById(long id) {
        return addressRepository.findById(id).orElseThrow( ()-> new ModelNotFoundException("Address not found."));
    }

    public Address save (Address address) {
        return addressRepository.save(address);
    }

    public List<Address> findAll () {
return addressRepository.findAll();
    }

    public void delete (Long id) {
        addressRepository.deleteById(id);
    }


}
