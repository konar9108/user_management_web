package com.dk.webapp.service;

import com.dk.webapp.model.Role;
import com.dk.webapp.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role findById(long id) {
        return roleRepository.findById(id).get();
    }

    public Role save (Role role) {
        return roleRepository.save(role);
    }



}
