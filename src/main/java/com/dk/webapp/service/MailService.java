package com.dk.webapp.service;

import com.dk.webapp.model.User;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailService {

    private final JavaMailSender emailSender;
    private final UserService userService;

    public MailService(JavaMailSender emailSender, UserService userService) {
        this.emailSender = emailSender;
        this.userService = userService;
    }


    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@konarusermanager.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }


    public String sendCreateUserMailAndReturnSuccessMsg(User user) {
        String subject = "Activation link - Konar User Manager";
        String link = String.format("http://localhost:8080/users/%d/activate", userService.findByLogin(user.getLogin()).getId());
        String content = String.format("To activate Your account click this link: %s", link);
        sendSimpleMessage(user.getEmail(), subject, content);

        return String.format("User with login %s was successfully created. Activation link has been sent to %s. It will expire in 1 hour.", user.getLogin(), user.getEmail());
    }
}