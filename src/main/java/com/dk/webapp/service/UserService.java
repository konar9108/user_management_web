package com.dk.webapp.service;

import com.dk.webapp.exception.ModelNotFoundException;
import com.dk.webapp.model.User;
import com.dk.webapp.repository.UserRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Supplier;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private Supplier<ModelNotFoundException> getModelNotFoundExceptionSupplier(String cause) {
        return () -> new ModelNotFoundException(cause);
    }

    private Supplier<ModelNotFoundException> getModelNotFoundExceptionSupplier() {
        return ModelNotFoundException::new;
    }

    public User findById(long id) {
        return userRepository.findById(id).orElseThrow(getModelNotFoundExceptionSupplier());
    }

    public List<User> findAll () {
        return userRepository.findAll();
    }

    public void delete (Long id) {
        userRepository.deleteById(id);
    }


    public User save (User user) {
        return userRepository.save(user);
    }

    public User findByIdWithAddresses (long id) {
        return userRepository.findByIdWithAddresses(id).orElseThrow(getModelNotFoundExceptionSupplier("Address not found"));
    }

    public List<User> findAll (int page, int size) {
        return userRepository.findAll(PageRequest.of(page, size)).getContent();
    }

    public List<User> findAllSortByLogin (int page, int size) {
        return userRepository.findAll(PageRequest.of(page,size, Sort.by("login"))).getContent();
    }

    public User findByLogin (String login) {
       return userRepository.findByLogin(login).orElseThrow(getModelNotFoundExceptionSupplier(String.format("User with login: %s not found", login)));
    }

    public User findByLastName(String lastName) {
        return userRepository.findByLastName(lastName).orElseThrow(getModelNotFoundExceptionSupplier(String.format("User with last name: %s not found", lastName)));
    }

//    public boolean updateUser (User user) {
//        if (user.getEmail() == null){
//            System.out.println("User not found.");
//            return false;
//        }
//        save(user);
//        return true;
//    }


}
