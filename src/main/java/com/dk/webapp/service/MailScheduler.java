package com.dk.webapp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MailScheduler {

    private final MailService mailService;

    public MailScheduler(MailService mailService) {
        this.mailService = mailService;
    }


//    @Scheduled(fixedRate = 30 * 1000)  wysyła co 30 sekund
    public void sendMail() {
        log.info("Sending email ...");
        mailService.sendSimpleMessage("konarusermanager@gmail.com", "test sub", "TEST BODY");


    }

}
