package com.dk.webapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SpringBootMain implements CommandLineRunner {


    private static void xSeparator() {
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXX");
    }

    @Override
    public void run(String... args) throws Exception {

    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMain.class, args);
    }

}