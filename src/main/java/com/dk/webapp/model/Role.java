package com.dk.webapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Role extends AbstractEntity {


    public enum RoleEnum {
        ADMIN, USER
    }

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleEnum role;


    public Role(String description, RoleEnum role) {
        this.description = description;
        this.role = role;
    }
}
