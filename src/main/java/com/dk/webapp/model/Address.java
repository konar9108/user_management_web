package com.dk.webapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity


public class Address extends AbstractEntity implements Comparable<Address> {

    @Column(nullable = false)
    private String city;
    @Column(nullable = false)
    private String country;
    @Column(nullable = false)
    private String street;
    @Column(nullable = false)
    private String zipCode;

    @JsonBackReference
    @ManyToMany(mappedBy = "addresses", cascade = CascadeType.DETACH)
    private Set<User> users = new HashSet<>();

    @PreRemove
    private void removeAssociations() {
        for (User u : users) {
            u.getAddresses().remove(this);
        }
    }



    public Address(String city, String country, String street, String zipCode) {
        this.city = city;
        this.country = country;
        this.street = street;
        this.zipCode = zipCode;

    }

    public Address() {
    }

    @Override
    public String toString() {
        return "Address: " +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", street='" + street + '\'' +
                ", zipCode='" + zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address = (Address) o;
        return Objects.equals(city, address.city) &&
                Objects.equals(country, address.country) &&
                Objects.equals(street, address.street) &&
                Objects.equals(zipCode, address.zipCode) &&
                Objects.equals(users, address.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(city, country, street, zipCode, users);
    }

    @Override
    public int compareTo(Address o) {
        return getCountry().compareTo(o.getCountry());

    }
}
