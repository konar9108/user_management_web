package com.dk.webapp.exception;

public class ModelNotFoundException extends RuntimeException {

    public ModelNotFoundException(String cause) {
        super(cause);
    }

    public ModelNotFoundException() {
        super("Entity not found.");
    }
}
