package com.dk.webapp.exception;

public class WrongPasswordException extends RuntimeException {

    public WrongPasswordException() {
        super("Password and confirm password didn't match.");
    }
}
