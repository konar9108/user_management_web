package com.dk.webapp.rest;


import com.dk.webapp.service.AddressService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/addresses")
public class AddressRestController {

    private final AddressService addressService;

    public AddressRestController(AddressService addressService) {
        this.addressService = addressService;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable Long id) {
        addressService.delete(id);
    }


//    @PutMapping("/{id}")
//    @ResponseStatus(HttpStatus.ACCEPTED)
//    public Address editAddress (@RequestBody Address address, @PathVariable Long id){
//
////User editedUser = userService.findById(id);
////
////editedUser.setEmail(user.getEmail());
////editedUser.setLastName(user.getLastName());
////editedUser.setFirstName(user.getFirstName());
////editedUser.setLogin(user.getLogin());
//
//       return new Address();
//    }


}
