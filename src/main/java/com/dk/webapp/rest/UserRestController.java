package com.dk.webapp.rest;


import com.dk.webapp.model.User;
import com.dk.webapp.service.AddressService;
import com.dk.webapp.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/rest/users")
public class UserRestController {

    private final UserService userService;
    private final AddressService addressService;

    public UserRestController(UserService userService, AddressService addressService) {
        this.userService = userService;
        this.addressService = addressService;
    }

    @GetMapping
    ArrayList<User> all() {
        return (ArrayList<User>) userService.findAll();
    }

    @GetMapping("/{id}")
    User one(@PathVariable Long id) {
        return userService.findById(id);
    }


    @DeleteMapping("/{id}/addresses/{addressId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable (name = "id") Long id, @PathVariable (name = "addressId") Long addressId) {
        User user = userService.findById(id);
        user.removeAddress(addressService.findById(addressId));
        userService.save(user);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteUserAddress(@PathVariable Long id) {
        userService.delete(id);
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public User editUser (@RequestBody User user, @PathVariable Long id){

User editedUser = userService.findById(id);

editedUser.setEmail(user.getEmail());
editedUser.setLastName(user.getLastName());
editedUser.setFirstName(user.getFirstName());
editedUser.setLogin(user.getLogin());
editedUser.setAddresses(user.getAddresses());

       return userService.save(editedUser);
    }

//    @PutMapping("/employees/{id}")
//    Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {
//
//        return repository.findById(id)
//                .map(employee -> {
//                    employee.setName(newEmployee.getName());
//                    employee.setRole(newEmployee.getRole());
//                    return repository.save(employee);
//                })
//                .orElseGet(() -> {
//                    newEmployee.setId(id);
//                    return repository.save(newEmployee);
//                });
//    }




}
