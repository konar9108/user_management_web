package com.dk.webapp.controller;

import com.dk.webapp.dto.CreateOrUpdateUserDto;
import com.dk.webapp.exception.WrongPasswordException;
import com.dk.webapp.mappers.UserMapper;
import com.dk.webapp.model.Address;
import com.dk.webapp.model.User;
import com.dk.webapp.service.AddressService;
import com.dk.webapp.service.MailService;
import com.dk.webapp.service.UserService;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

@RequestMapping("/users")
@Controller
public class UserController {

    private final UserService userService;
    private final AddressService addressService;
    private final MailService emailService;


    public UserController(UserService userService, AddressService addressService, MailService emailService) {
        this.userService = userService;
        this.addressService = addressService;
        this.emailService = emailService;
    }

    @InitBinder
    public void allowEmptyDateBinding(WebDataBinder binder) {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }


    @GetMapping
    public String users(Model model, @ModelAttribute("successMsg") String successMsg) {
        model.addAttribute("users", userService.findAll());
        model.addAttribute("successMsg", successMsg);
        return "users";
    }


    //CREATE

    @GetMapping("/create")
    public String createForm(Model model) {
        model.addAttribute("dto", new CreateOrUpdateUserDto());
        return "createUser";
    }

    @PostMapping("/create")
    public ModelAndView createUser(@ModelAttribute CreateOrUpdateUserDto userDto, Model model) {
        if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            throw new WrongPasswordException();
        }
        User user = UserMapper.INSTANCE.toEntity(userDto);
        try {
            userService.save(user);
        } catch (DataIntegrityViolationException | ConstraintViolationException ex) {
            throw new RuntimeException("User with same login already exists.");
        }
        String successMsg = emailService.sendCreateUserMailAndReturnSuccessMsg(user);
        model.addAttribute("successMsg", successMsg);

        return new ModelAndView("redirect:/users", model.asMap());
    }

    // END CREATE

    //UPDATE

    @GetMapping("/{userId}")
    public String updateForm(Model model, @PathVariable("userId") Long id) {
        CreateOrUpdateUserDto user = UserMapper.INSTANCE.toDto(userService.findByIdWithAddresses(id));
        Collections.sort(user.getAddresses());
        List<Address> addressLinkedList = new LinkedList<>(addressService.findAll());
        addressLinkedList.removeAll(user.getAddresses());
        addressLinkedList.sort(Comparator.comparing(Address::getCountry));
        model.addAttribute("addressLinkedList", addressLinkedList);
        model.addAttribute("user", user);
        model.addAttribute("addressAdd", new Address());
        return "updateUser";
    }

    @PostMapping("/{id}")
    public ModelAndView editUser(@ModelAttribute("user") User user, @PathVariable Long id) {
        User editedUser = userService.findById(id);
        editedUser.setLogin(user.getLogin());
        editedUser.setFirstName(user.getFirstName());
        editedUser.setLastName(user.getLastName());
        editedUser.setEmail(user.getEmail());
        userService.save(editedUser);

        return new ModelAndView("redirect:/users");
    }

    @PostMapping("/{id}/address/add")
    public ModelAndView addAddressToUser(@ModelAttribute("address") Address address, @PathVariable Long id) {

        User user = userService.findById(id);
        Address address1 = addressService.findById(address.getId());
        user.addAddress(address1);
        userService.save(user);
        return new ModelAndView("redirect:/users/" + id);
    }

// END UPDATE

// ACTIVATION LINK

    @GetMapping("/{id}/activate")
    public ModelAndView activateLink(@PathVariable Long id, Model model) {

        User user = userService.findById(id);

        if (user.isActivated()) {
            model.addAttribute("successMsg", String.format("account with login: %s has already been activated.", user.getLogin()));
            return new ModelAndView("redirect:/index", model.asMap());
        }

        LocalDateTime expiryTime = user.getCreationDate().plusHours(1);
//        LocalDateTime expiryTime = user.getCreationDate().plusSeconds(5);
         if (LocalDateTime.now().isAfter(expiryTime)) {
            model.addAttribute("successMsg", "Link has expired.");
        } else  {
            user.setActivated(true);
            userService.save(user);
            model.addAttribute("successMsg", String.format("account with login: %s has been activated.", user.getLogin()));
        }
        return new ModelAndView("redirect:/index", model.asMap());
    }


}
