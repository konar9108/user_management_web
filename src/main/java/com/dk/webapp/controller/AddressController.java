package com.dk.webapp.controller;

import com.dk.webapp.model.Address;
import com.dk.webapp.service.AddressService;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/addresses")
@Controller
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @InitBinder
    public void allowEmptyDateBinding( WebDataBinder binder )
    {
        // tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor( String.class, new StringTrimmerEditor( true ));
    }

    @GetMapping
    public String addresses(Model model, @ModelAttribute("successMsg") String successMsg) {
        model.addAttribute("addresses", addressService.findAll());
        model.addAttribute("successMsg", successMsg);
        return "address/addresses";
    }


    @GetMapping("/create")
    public String createForm(Model model) {
        model.addAttribute("address", new Address());
        return "address/createAddress";
    }

    @PostMapping("/create")
    public ModelAndView addAddress(@ModelAttribute Address address, Model model) {
        addressService.save(address);
        model.addAttribute("successMsg", ("Address was successfully created."));

        return new ModelAndView("redirect:/addresses", model.asMap());
    }

    @GetMapping("/{id}")
    public String editForm(@PathVariable Long id,Model model) {
        model.addAttribute("address", addressService.findById(id));
        return "address/updateAddress";
    }


    @PostMapping("/{id}")
    public ModelAndView editAddress(@ModelAttribute("address") Address address, @PathVariable Long id) {

        Address editedAddress = addressService.findById(id);

        editedAddress.setCity(address.getCity());
        editedAddress.setCountry(address.getCountry());
        editedAddress.setStreet(address.getStreet());
        editedAddress.setZipCode(address.getZipCode());

        addressService.save(editedAddress);

        return new ModelAndView("redirect:/addresses");
    }

}
