package com.dk.webapp.controller;

import org.hibernate.PropertyValueException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.SendFailedException;

/**
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class CustomExceptionController {


    @ExceptionHandler({NullPointerException.class, PropertyValueException.class})
    public ModelAndView handleNullPointerExc (Model model) {
        model.addAttribute("errorMsg", "Cannot submit with empty field.");
        return new ModelAndView("error/error-500", model.asMap());
    }

    @ExceptionHandler(SendFailedException.class)
    public ModelAndView handleMailSendFail (Model model, SendFailedException sfEx) {
        model.addAttribute("errorMsg", "Mail has not been send. \n " + sfEx.getMessage());
        return new ModelAndView("error/error-500", model.asMap());
    }



}
