package com.dk.webapp.controller;

import org.springframework.core.annotation.Order;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */
@Order
@ControllerAdvice
public class RuntimeExceptionController {

    @ExceptionHandler(RuntimeException.class)
    public ModelAndView handleInternalServerException (Model model, RuntimeException ex) {
        model.addAttribute("errorMsg", ex.getMessage());
        return new ModelAndView("error/error-500", model.asMap());
    }

}
