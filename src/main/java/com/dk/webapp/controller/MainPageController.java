package com.dk.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/index")
public class MainPageController {


    @GetMapping
    public String index(Model model, @ModelAttribute("successMsg") String successMsg) {
        model.addAttribute("successMsg", successMsg);
        return "index";
    }


}
