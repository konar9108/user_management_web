package com.dk.webapp.repository;

import com.dk.webapp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin (String login);

    Optional<User> findByLastName (String lastName);


    @Query("SELECT u FROM User u left join fetch u.addresses where u.id =:user_id")
    Optional<User> findByIdWithAddresses (@Param("user_id")Long id);






}
