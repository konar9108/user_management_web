package com.dk.webapp.repository;

import com.dk.webapp.model.Address;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends PagingAndSortingRepository<Address, Long> {

    public List<Address> findAll ();


}
