
function closeModal() {
    $("#successModal")
        .toggle(false);
}

function showOrHideSuccessModal(successMsg) {
    return successMsg.length > 0;
}

function initForm() {
    $('#successModal')
        .toggle(showOrHideSuccessModal($('#successMsg').val()));

}


$(document).ready(() => {
    initForm();
});

