
$(document).ready(() => {

    $(".button-delete-user-address").click((event) => {
        console.info('Element clicked with id: ', event.target.id);
        const id = event.target.getAttribute('data-id');
        const addressId = event.target.getAttribute('data-address-id');
        $.ajax(
            {
                url: '/rest/users/' + id + '/addresses/' + addressId,
                method: 'DELETE'
            }
        )
            .then(() => {
                console.log('Element was deleted: ', id);
                window.location.href = '/users/' + id;
            })
            .catch((error) => {
                    console.error('Error during user deletion: ', error)
                }
            )

    });


    $(".user-address-select").change(function (){
        if ($(this).find('option:selected').val() === '0')
            $(".btn-user-put").attr('disabled', true)
        else
            $(".btn-user-put").attr('disabled', false)
    }).trigger('change')


});

