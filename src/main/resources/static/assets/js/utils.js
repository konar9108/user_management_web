function onEditClicked(userId) {
    window.location.href = '/users/' + userId;
}

function onEditClickAdr(addressId) {
    window.location.href = '/addresses/' + addressId;
}



$(document).ready(() => {

    $(".button-delete-user").click((event) => {
        console.info('Element clicked with id: ', event.target.id);
        const id = event.target.getAttribute('data-id');
        $.ajax(
            {
                url: '/rest/users/' + id,
                method: 'DELETE'
            }
        )
            .then(() => {
                console.log('Element was deleted: ', id);
                window.location.href = '/users';
            })
            .catch((error) => {
                    console.error('Error during user deletion: ', error)
                }
            )

    });

    $(".button-delete-address").click((event) => {
        console.info('Element clicked with id: ', event.target.id);
        const id = event.target.getAttribute('data-id');
        $.ajax(
            {
                url: '/rest/addresses/' + id,
                method: 'DELETE'
            }
        )
            .then(() => {
                console.log('Element was deleted: ', id);
                window.location.href = '/addresses';
            })
            .catch((error) => {
                    console.error('Error during address deletion: ', error)
                }
            )

    });

});

