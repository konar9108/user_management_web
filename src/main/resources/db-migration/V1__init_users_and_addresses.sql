INSERT INTO user (login, first_name, last_name, email, version, creation_date)
VALUES
('konarro', 'damian', 'konarowski', 'konar@konar.pl', 1, NOW()),
('bodzio', 'bogdan', 'bogdanovic', 'bogdan@bogdan.pl', 1, NOW()),
('adamko', 'adam', 'adamovic', 'adam@adam.pl', 1, NOW());

INSERT INTO Address (city, country, street, zip_code, version, creation_date)
VALUES
('bialystok','poland','malmeda 1', '15-001', 1, NOW()),
('bialystok','poland','rycerska 1', '15-157', 1, NOW()),
('bialystok','poland','waszyngtona 1', '15-169', 1, NOW()),
('new york','USA','wall street 1', '00-001', 1, NOW());

