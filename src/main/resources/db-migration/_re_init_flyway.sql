-- use it on Your db in case flyway won't let Your application start.

CREATE DEFINER=`PUT YOUR DB USERNAME HERE` PROCEDURE `RE_INIT_FLYWAY`()
BEGIN
DELETE FROM flyway_schema_history WHERE installed_rank > 0;
DELETE FROM user_address
where user_id > 0;
DELETE FROM user where id > 0;
DELETE FROM address where id > 0;
ALTER TABLE `spring_boot`.`address` AUTO_INCREMENT = 1 ;
ALTER TABLE `spring_boot`.`user` AUTO_INCREMENT = 1;
END